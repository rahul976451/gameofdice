package com.example.game;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class FirstActivity extends AppCompatActivity {

    EditText target;
    RadioGroup noOfPlayers;
    RadioButton player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }


        target = findViewById(R.id.et_target);

    }

    public void start(View view) {

        noOfPlayers = findViewById(R.id.noOfPlayers);
        int playerId = noOfPlayers.getCheckedRadioButtonId();
        player = findViewById(playerId);
        String string = player.getText().toString();

        if(target.getText().toString().isEmpty()) {
            target.setError("Please enter a valid number");
            return;
        }

        int winScore = Integer.parseInt(target.getText().toString());
        if(winScore < 7 || winScore > 100){

            target.setText("");
            target.setError("Please enter number between 25 and 100");

        }
        else{
            Intent game = new Intent(this,MainActivity.class);
            game.putExtra("Score",target.getText().toString());
            game.putExtra("Players",Integer.parseInt(string));
            startActivity(game);
        }

    }
}
