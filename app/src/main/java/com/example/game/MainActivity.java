package com.example.game;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    ImageView dice;
    TextView player1,player2,player3,player4,temptextView;
    LinearLayout  player_1,player_2,player_3,player_4;
    Random random;
    ShapeDrawable shape;
    static int playerturn = 0;
    int j=0,k=0,val=0;
    Handler handler = new Handler();
    TextView totalScore;
    int total = 0, noOfPlayers = 0;
    String score;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        totalScore = findViewById(R.id.score);

        Intent intent = getIntent();
        score = intent.getStringExtra("Score");
        noOfPlayers = intent.getIntExtra("Players",2);
        totalScore.setText(score);

        total=Integer.parseInt(totalScore.getText().toString());

        dice = findViewById(R.id.dice);
        player1 = findViewById(R.id.p1score);
        player2 = findViewById(R.id.p2score);
        player3 = findViewById(R.id.p3score);
        player4 = findViewById(R.id.p4score);

        player1.setText("0");
        player2.setText("0");
        player3.setText("0");
        player4.setText("0");

        player_1 = findViewById(R.id.llp1);
        player_2 = findViewById(R.id.llp2);
        player_3 = findViewById(R.id.llp3);
        player_4 = findViewById(R.id.llp4);

        random = new Random();

        shape = new ShapeDrawable();
        shape.setShape(new RectShape());
        shape.getPaint().setColor(Color.GREEN);
        shape.getPaint().setStrokeWidth(10f);
        shape.getPaint().setStyle(Paint.Style.STROKE);
        player_1.setBackground(shape);


        if(noOfPlayers == 2){
            player_3.setVisibility(LinearLayout.GONE);
            player_4.setVisibility(LinearLayout.GONE);
        }

        else if(noOfPlayers == 3){
            player_4.setVisibility(LinearLayout.GONE);
        }

        else {
            ;
        }

        dice.setOnClickListener(new View.OnClickListener() {
            int i;
            @Override
            public void onClick(View v) {
                dice.setEnabled(false);
                switch (playerturn){
                    case 0:
                        i=(random.nextInt(1000))%6+1;
                        rollingDiceAnimation();
                        rollDice(i,player1);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                player_1.setBackground(null);
                                player_2.setBackground(shape);
                                dice.setEnabled(true);
                            }
                        },2000);

                        break;
                    case 1:
                        i=(random.nextInt(1000))%6+1;
                        rollingDiceAnimation();
                        rollDice(i,player2);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(noOfPlayers>2){
                                    player_3.setBackground(shape);
                                    player_2.setBackground(null);
                                }
                                else {
                                    player_1.setBackground(shape);
                                    player_2.setBackground(null);
                                }
                                dice.setEnabled(true);
                            }
                        },2000);
                        break;
                    case 2:
                        i=(random.nextInt(1000))%6+1;
                        rollingDiceAnimation();
                        rollDice(i,player3);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(noOfPlayers>3){
                                    player_4.setBackground(shape);
                                    player_3.setBackground(null);
                                }
                                else {
                                    player_1.setBackground(shape);
                                    player_3.setBackground(null);
                                }
                                dice.setEnabled(true);
                            }
                        },2000);
                        break;
                    case 3:
                        rollingDiceAnimation();
                        i=(random.nextInt(1000))%6+1;
                        rollDice(i,player4);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                player_1.setBackground(shape);
                                player_4.setBackground(null);
                                dice.setEnabled(true);
                            }
                        },2000);
                        break;
                }
            }
        });
    }

    public void rollDice(int i, TextView textView)
    {
        String value = textView.getText().toString();
        val = Integer.parseInt(value);
        temptextView = textView;
        switch (i){
            case 1:
                dice.setImageResource(R.drawable.dice1);
                if(val==0){
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                        temptextView.setText(Integer.toString(val+1));
                        }
                        },2000);
                    }
                else if(val+1==total)
                {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            temptextView.setText(Integer.toString(val+1));
                            //write intent for winner
                        }
                    },2000);
                }
                else{
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            temptextView.setText(Integer.toString(val+1));
                        }
                    },2000);
                }
                break;
            case 2:
                dice.setImageResource(R.drawable.dice2);
                if(val == 0 || (val<total && val+2>total))
                {
                    ;
                }
                else if(val<total && val+2==total)
                {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            temptextView.setText(Integer.toString(val+2));
                            //write intent for winner
                        }
                    },2000);
                }
                else{
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            temptextView.setText(Integer.toString(val+2));
                        }
                    },2000);
                }
                break;
            case 3:
                dice.setImageResource(R.drawable.dice3);
                if(val == 0 || (val<total && val+3>total))
                {
                    ;
                }
                else if(val<total && val+3==total)
                {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            temptextView.setText(Integer.toString(val+3));
                            //write intent for winner
                        }
                    },2000);
                }
                else{
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            temptextView.setText(Integer.toString(val+3));
                        }
                    },2000);
                }
                break;
            case 4:
                dice.setImageResource(R.drawable.dice4);
                if(val == 0 || (val<total && val+4>total))
                {
                    ;
                }
                else if(val<total && val+4==total)
                {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            temptextView.setText(Integer.toString(val+4));
                            //write intent for winner
                        }
                    },2000);
                }
                else{
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            temptextView.setText(Integer.toString(val+4));
                        }
                    },2000);
                }
                break;
            case 5:
                dice.setImageResource(R.drawable.dice5);
                if(val == 0 || (val<total && val+5>total))
                {
                    ;
                }
                else if(val<total && val+5==total)
                {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            temptextView.setText(Integer.toString(val+5));
                            //write intent for winner
                        }
                    },2000);
                }
                else{
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            temptextView.setText(Integer.toString(val+5));
                        }
                    },2000);
                }
                break;
            case 6:
                dice.setImageResource(R.drawable.dice6);
                if(val<total && val+6>total)
                {
                    ;
                }
                else if(val<total && val+6==total)
                {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            temptextView.setText(Integer.toString(val+6));
                            //write intent for winner
                        }
                    },2000);
                }
                else{
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            temptextView.setText(Integer.toString(val+6));
                        }
                    },2000);
                }
                break;
        }
    }

    public void rollingDiceAnimation()
    {
        Animation rotate = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotation);
        dice.startAnimation(rotate);
        if(noOfPlayers == 2){
            playerturn=(playerturn + 1 )%2;
        }
        else if(noOfPlayers == 3){
            playerturn=(playerturn + 1 )%3;
        }
        else {
            playerturn=(playerturn + 1 )%4;
        }

    }

    @Override
    public void onBackPressed() {
        final MaterialDialog.Builder materialDialog=new MaterialDialog.Builder(MainActivity.this);
        materialDialog.title("Do you really want to exit");
        materialDialog.positiveText("Yes");
        materialDialog.negativeText("No");
        materialDialog.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                finish();
            }
        });
        materialDialog.onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

            }
        });
        materialDialog.show();
        //finish();
    }
}
